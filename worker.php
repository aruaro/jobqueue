<?php

require_once __DIR__ . '/vendor/autoload.php';

include __DIR__.'/config.php';

$c['logger.channel'] = 'GRUNT|WORKER';
$c['logger.path']    = 'php://stdout';

$c->register(new \Ecw\Grunt\Events\EventServiceProvider());
$c->register(new \Ecw\Grunt\Workers\WorkerServiceProvider());

try {
    $c['queue.worker']->listen();
} catch(\Exception $e) {
    $c['logger']->handleException($e);
}

// $c['queue.worker']->flushAll();

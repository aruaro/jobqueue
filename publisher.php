<?php

require_once __DIR__ . '/vendor/autoload.php';

include __DIR__.'/config.php';

$c['logger.channel'] = 'GRUNT|WORKER';
$c['logger.path']    = 'php://stdout';

$c->register(new \Ecw\Grunt\Events\EventServiceProvider());
$c->register(new \Ecw\Grunt\Publishers\PublisherServiceProvider());

// {"params":{"env":{"HTTP_HOST":"ecw.dev","CC_ENV":"dev","REQUEST_METHOD":"CLI","_POST":"msg=4688"},"script":"/vagrant/devops/grunt-v2/clitask.php","show_log":true}}

$dispatcher = \FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) use($c) {
    $r->addRoute('GET', '/ping', function() {
        echo json_encode(['result' => 'success']);
    });

    $r->addRoute('POST', '/publish', function($data) use($c) {
        // PUBLISH TASK TO QUEUE
        $success = $c['queue.publisher']->send(
            '\\Ecw\\Grunt\\Tasks\\PhpCliTask',
            $data['params']
        );

        if($success === true) {
            echo json_encode(['result' => 'success']);
        }
    });
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri        = $_SERVER['REQUEST_URI'];
$uri        = str_replace('/publisher.php', '', $_SERVER['REQUEST_URI']);
$jsonData   = [];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

// Check if request body is JSON
if($_SERVER['CONTENT_TYPE'] == 'application/json') {
    try {
        $jsonData = file_get_contents("php://input");
        $jsonData = json_decode($jsonData, true);
    } catch(\Exception $e) {
        $c['logger']->handleException($e);
    }
}

$uri       = rawurldecode($uri);
$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

header('Content-Type: application/json');

switch ($routeInfo[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'result' => 'error',
            'error'  => [
                'code'   => 404,
                'reason' => 'Not Found'
            ]
        ]);
        break;
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        header('HTTP/1.0 405 Method Not Allowed');
        echo json_encode([
            'result' => 'error',
            'error'  => [
                'code'   => 405,
                'reason' => 'Method Not Allowed'
            ]
        ]);
        break;
    case \FastRoute\Dispatcher::FOUND:
        call_user_func($routeInfo[1], $jsonData);
        break;
}

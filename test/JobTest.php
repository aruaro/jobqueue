<?php

namespace Ecw\Grunt\Test;

use Ecw\Grunt\Jobs\Job;
use Ecw\Grunt\Tasks\Task;


class JobTest extends \PHPUnit_Framework_TestCase
{
    public function testTaskData() {
        $job = new Job();

        $job->setTaskName('\\Ecw\\Grunt\\Tasks\\Task');
        $job->setTaskData([]);

        $this->assertSame([], $job->getTaskData());
    }

    public function testResolveTask() {
        $job = new Job();

        $job->setTaskName('\\Ecw\\Grunt\\Tasks\\Task');
        $job->setTaskData([]);

        $this->assertTrue($job->resolveTask());
        $this->assertInstanceOf('\\Ecw\\Grunt\\Tasks\\Task', $job->getTaskCallable());
    }
}

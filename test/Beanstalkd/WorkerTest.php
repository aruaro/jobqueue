<?php

namespace Ecw\Grunt\Test\Beanstalkd;

use Ecw\Grunt\Test\WorkerTest;
use Ecw\Grunt\Workers\BeanstalkdWorker;


class BeanstalkdWorkerTest extends WorkerTest
{
    public function testListen() {

    }

    public function testPop() {
        $mockBeanstalkd = $this->getMockBeanstalkd();
        $mockConnector  = $this->getMockConnector($mockBeanstalkd);

        $mockJob = $this->getMockBuilder('\\Ecw\\Grunt\\Jobs\\Job')
                        ->setMethods(['getPheanstalkJob', 'resolveTask', 'execute'])
                        ->getMock();

        $mockBeanstalkdJob = $this->getMockBuilder('\\Pheanstalk\\Job')
                                ->disableOriginalConstructor()
                                ->getMock();

        $mockJob->method('resolveTask')
            ->will($this->onConsecutiveCalls(true, false));

        $mockJob->method('execute')
            ->will($this->onConsecutiveCalls(true, false));

        $mockJob->method('getPheanstalkJob')
            ->willReturn($mockBeanstalkdJob);

        $worker = new BeanstalkdWorker($mockConnector, 'test');

        $this->assertTrue($worker->pop($mockJob));
        $this->assertFalse($worker->pop($mockJob));
    }

    private function getMockConnector($mockBeanstalkd) {
        $mockConnector  = $this->getMockBuilder(
                                '\\Ecw\\Grunt\\Connectors\\BeanstalkdConnector'
                            )
                            ->disableOriginalConstructor()
                            ->setMethods(['setConnector', 'getConnection'])
                            ->getMock();

        $mockConnector->expects($this->once())
            ->method('setConnector')
            ->with($this->isInstanceOf('\\Pheanstalk\\Pheanstalk'));

        $mockConnector->method('getConnection')
            ->willReturn($mockBeanstalkd);

        $mockConnector->setConnector($mockBeanstalkd);

        return $mockConnector;
    }

    private function getMockBeanstalkd() {
        $mockBeanstalkd = $this->getMockBuilder('\\Pheanstalk\\Pheanstalk')
                            ->disableOriginalConstructor()
                            ->setMethods(['delete', 'release'])
                            ->getMock();

        $mockBeanstalkd->expects($this->any())
            ->method('delete')
            ->with($this->isInstanceOf('\\Pheanstalk\\Job'));

        $mockBeanstalkd->expects($this->any())
            ->method('release')
            ->with($this->isInstanceOf('\\Pheanstalk\\Job'));

        return $mockBeanstalkd;
    }
}

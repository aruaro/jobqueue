<?php

namespace Ecw\Grunt\Test\Beanstalkd;

use Ecw\Grunt\Test\PublisherTest;
use Ecw\Grunt\Publishers\BeanstalkdPublisher;
use Ecw\Grunt\Jobs\BeanstalkdJob;


class BeanstalkdPublisherTest extends PublisherTest
{
    public function _testSend() {
        // not doing this right now
        $mockJob = $this->getMockBuilder('\\Ecw\\Grunt\\Jobs\\Job')
                        ->setMethods(['setTaskName', 'setTaskData'])
                        ->getMock();

        $mockJob->expects($this->once())
            ->method('setTaskName')
            ->with($this->isType('string'));

        $mockJob->expects($this->once())
            ->method('setTaskData')
            ->with($this->isType('string'));

        $mockJob->method('validateJob')->willReturn(true);
        $mockJob->method('resolveTask')->willReturn(true);
    }

    public function testPush() {
        $mockBeanstalkd = $this->getMockBeanstalkd();
        $mockConnector  = $this->getMockConnector($mockBeanstalkd);

        $job = new BeanstalkdJob();

        $job->setTaskName('');
        $job->setTaskData([]);

        $publisher = new BeanstalkdPublisher($mockConnector, 'test');

        $this->assertInternalType('integer', $publisher->push($job));
    }

    private function getMockConnector($mockBeanstalkd) {
        $mockConnector  = $this->getMockBuilder(
                                '\\Ecw\\Grunt\\Connectors\\BeanstalkdConnector'
                            )
                            ->disableOriginalConstructor()
                            ->setMethods(['setConnector', 'getConnection'])
                            ->getMock();

        $mockConnector->expects($this->once())
            ->method('setConnector')
            ->with($this->isInstanceOf('\\Pheanstalk\\Pheanstalk'));

        $mockConnector->method('getConnection')
            ->willReturn($mockBeanstalkd);

        $mockConnector->setConnector($mockBeanstalkd);

        return $mockConnector;
    }

    private function getMockBeanstalkd() {
        $mockBeanstalkd = $this->getMockBuilder('\\Pheanstalk\\Pheanstalk')
                            ->disableOriginalConstructor()
                            ->setMethods(['useTube', 'put'])
                            ->getMock();

        $mockBeanstalkd->expects($this->once())
            ->method('useTube')
            ->with($this->isType('string'))
            ->willReturn($mockBeanstalkd);

        $mockBeanstalkd->expects($this->once())
            ->method('put')
            ->with(
                $this->isType('string'),
                $this->anything(),
                $this->anything(),
                $this->anything()
            )
            ->willReturn(1);

        return $mockBeanstalkd;
    }
}

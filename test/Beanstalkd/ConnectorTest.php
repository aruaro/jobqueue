<?php

namespace Ecw\Grunt\Test\Beanstalkd;

use Ecw\Grunt\Connectors\BeanstalkdConnector;
use Ecw\Grunt\Test\ConnectorTest;


class BeanstalkdConnectorTest extends ConnectorTest
{
    public function testGetConnectorName() {
        $connector = new BeanstalkdConnector();

        $this->assertEquals('Beanstalkd', $connector->getConnectorName());
    }

    public function testConnectionSuccess() {
        // this feels like a useless test
        $mockBeanstalkd = $this->getMockBuilder('\\Pheanstalk\\Pheanstalk')
                            ->disableOriginalConstructor()
                            ->getMock();

        $connector = new BeanstalkdConnector();

        $connector->setConfig($this->getConnectorConfig());
        $connector->setConnection($mockBeanstalkd);
        $connector->connect();

        $this->assertInstanceOf('\\Pheanstalk\\Pheanstalk', $connector->getConnection());
    }

    public function testConnectionFail() {
        // this feels like another useless test
        $connector = new BeanstalkdConnector();

        $connector->setConfig($this->getConnectorConfig());
        // $connector->connect();

        $this->assertNull($connector->getConnection());
    }

    public function testDisconnectSuccess() {
        $mockBeanstalkd = $this->getMockBuilder('\\Pheanstalk\\Pheanstalk')
                            ->disableOriginalConstructor()
                            ->getMock();

        $connector = new BeanstalkdConnector();

        $connector->setConfig($this->getConnectorConfig());
        $connector->setConnection($mockBeanstalkd);
        $connector->connect();

        $this->assertInstanceOf('\\Pheanstalk\\Pheanstalk', $connector->getConnection());

        $connector->disconnect();

        $this->assertNull($connector->getConnection());
    }

    private function getConnectorConfig() {
        return [
            'name'               => 'test',
            'host'               => '10.1.1.102',
            'port'               => 11300,
            'connection_timeout' => 30,
            'is_persistent'      => true
        ];
    }
}

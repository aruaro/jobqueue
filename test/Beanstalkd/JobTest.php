<?php

namespace Ecw\Grunt\Test\Beanstalkd;

use Ecw\Grunt\Test\JobTest;
use Ecw\Grunt\Jobs\BeanstalkdJob;


class BeanstalkdJobTest extends JobTest
{
    public function testJobData() {
        $job = new BeanstalkdJob();

        $job->setTaskName('\\Ecw\\Grunt\\Tasks\\Task');
        $job->setTaskData([]);

        $this->assertEquals(
            '{"taskClass":"\\\\Ecw\\\\Grunt\\\\Tasks\\\\Task","taskData":[]}',
            $job->getJobData()
        );
    }

    public function testExecute() {
        $mockTask = $this->getMockBuilder('\\Ecw\\Grunt\\Tasks\\Task')
                        ->setMethods(['execute'])
                        ->getMock();

        $mockTask->expects($this->once())
            ->method('execute')
            ->with([])
            ->will($this->returnValue(true));

        $job = new BeanstalkdJob();

        $job->setTaskData([]);
        $job->setTaskCallable($mockTask);

        $this->assertTrue($job->execute());
    }
}

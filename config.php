<?php

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
ini_set('html_errors', false);
ini_set('memory_limit', '256M');
ini_set('xdebug.var_display_max_depth', 100);
ini_set('xdebug.default_enable', false);

require_once __DIR__ . '/vendor/autoload.php';

use Pimple\Container;

$c = new Container();

$c['config.queue'] = function() {
    $config = [
        'type'               => 'beanstalkd',
        'name'               => 'test',
        'port'               => 11300,
        'connection_timeout' => 30,
        'is_persistent'      => true,
        'listen_delay'       => 1000000
    ];

    // $config = [
    //     'type'         => 'rabbitmq',
    //     'name'         => 'test',
    //     'host'         => 'localhost',
    //     'port'         => 5672,
    //     'username'     => 'guest',
    //     'password'     => 'guest',
    //     'listen_delay' => 500000
    // ];

    switch($_SERVER['CC_ENV']) {
        case 'UAT':
            $config ['host'] = 'grunt-publisher-uat.ap-northeast-1.dm-prod';
        break;

        default:
            $config['host'] = 'grunt-publisher.dev';
        break;
    }

    return $config;
};

$c['logger.processors'] = function() { return []; };
$c['logger.handlers']   = function() {
    $handlers = [];
    $sentry   = new Raven_Client(sprintf(
        'https://%s:%s@app.getsentry.com/%s',
        '20afe82041804454acc34d9815a6956b',
        '335cbe9869ca47859d8bf526b1cb0be1',
        '72435'
    ));

    // $handlers[] = [
    //     'handler'    => new \Monolog\Handler\RavenHandler($sentry),
    //     'formatters' => [
    //         new Monolog\Formatter\LineFormatter("%message%\n", null, true, true)
    //     ]
    // ];

    return $handlers;
};


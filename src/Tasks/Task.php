<?php

namespace Ecw\Grunt\Tasks;

use Ecw\Grunt\Events\EventListener;


class Task implements TaskInterface
{
    /**
     * Logger
     * @var  EventListener
     */
    protected $logger;

    /**
     * Sets the event logger and listener
     */
    public function setLogger(EventListener $logger) {
        $this->logger = $logger;
    }

    /**
     * Overrides the default queue (special cases)
     * @return  string
     */
    public function getQueueName() {
        return false;
    }

    /**
     * Runs the background task.
     * The only required function for a background task processor.
     * @return  array  $params
     */
    public function execute(array $params) {
        return true;
    }
}

<?php

namespace Ecw\Grunt\Tasks;

use Symfony\Component\Process\Process;


class PhpCliTask extends Task
{
    protected $process;
    protected $logPath;


    /**
     * Get task delay in microseconds (Default: 500ms)
     */
    public function getProcessDelay() {
        return 1000000;
    }

    /**
     * Execute PHP CLI task
     * @param   array   $params  task parameters
     */
    public function execute(array $params = []) {
        if($this->startProcess($params) !== true) {
            return false;
        }

        while($this->process->isRunning()) {
            if(isset($params['show_log']) && $params['show_log'] === true) {
                $this->logger->fire(
                    new \Ecw\Grunt\Events\TaskOutputEvent(
                        $this,
                        [
                            'stdout' => $this->process->getIncrementalOutput(),
                            'stderr' => $this->process->getIncrementalErrorOutput(),
                        ]
                    )
                );
            }

            if($this->interlude($params) !== true) {
                // restart the process? no?
                // fire event
                break;
            }

            usleep($this->getProcessDelay());
        }

        return $this->fireProcessStatusEvent();
    }

    /**
     * Start the PHP CLI task
     * @param   array   $params
     */
    protected function startProcess(array $params = []) {
        try {
            if(!isset($params['script']) || empty($params['script'])) {
                throw new \InvalidArgumentException('Task PHP script required.');

                return false;
            }

            if(!file_exists($params['script'])) {
                throw new \RuntimeException('Task PHP script does not exist. '. $params['script']);

                return false;
            }

            $this->process = new Process(
                'php '.$params['script'],
                (isset($params['cwd']) && !empty($params['cwd'])) ? $params['cwd'] : '',
                (isset($params['env']) && !empty($params['env'])) ? $params['env'] : []
            );

            $this->process->start();

        } catch(Symfony\Component\Process\Exception\RuntimeException $e) {
            $this->logger->handleException($e);

            return false;
        } catch(\InvalidArgumentException $e) {
            $this->logger->handleException($e);

            return false;
        }catch(\RuntimeException $e) {
            $this->logger->handleException($e);

            return false;
        }

        return true;
    }

    /**
     * Execute other tasks in between the current running task
     * This includes pinging for MySQL timeouts, etc.
     * @param   array   $params  task parameters
     */
    private function interlude(array $params = []) {
        return true;
    }

    /**
     * Fire events about the processed task
     */
    private function fireProcessStatusEvent() {
        $success = false;
        $event   = null;

        if($this->process->isSuccessful()) {
            $event   = 'TaskFinishedEvent';
            return true;
        } elseif($this->process->hasBeenStopped()) {
            $event   = 'TaskStoppedEvent';
            $success = false;
        } elseif($this->process->hasBeenSignaled()) {
            $event   = 'TaskSignaledEvent';
            $success = false;
        } else {
            $event   = 'TaskTerminatedEvent';
            $success = false;
        }

        $event = '\\Ecw\\Grunt\\Events\\'.$event;

        $this->logger->fire(
            new $event($this),
            [
                'code'   => $this->process->getExitCode(),
                'stdout' => $this->process->getOutput(),
                'stderr' => $this->process->getErrorOutput(),
            ]
        );

        return $success;
    }
}

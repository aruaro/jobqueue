<?php

namespace Ecw\Grunt\Tasks;


interface TaskInterface
{
    /**
     * Overrides the default queue (special cases)
     * @return  string
     */
    public function getQueueName();

    /**
     * Runs the background task.
     * The only required function for a background task processor.
     * @return  array  $params
     */
    public function execute(array $params);
}

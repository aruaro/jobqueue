<?php

namespace Ecw\Grunt\Connectors;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;


class RabbitMQConnector implements ConnectorInterface
{
    /**
     * RabbitMQ connection
     * @var  AMQPStreamConnection
     */
    protected $connection;

    /**
     * RabbitMQ channel to use
     * @var  AMQPChannel
     */
    protected $channel;

    /**
     * Beanstalkd configuration
     * @var  mixed
     */
    protected $config;

    /**
     * Get name of queue service
     */
    public function getConnectorName() {
        return 'RabbitMQ';
    }

    /**
     * Get cofnig of queue service
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Set config of queue service
     */
    public function setConfig(array $config) {
        $this->config = $config;
    }

    /**
     * Get connection to queue service (instantiated object of library)
     */
    public function getConnection() {
        return $this->channel;
    }

    public function setConnection(AMQPChannel $channel) {
        $this->channel = $channel;
    }

    /**
     * Establishes queue connection
     * @param  array  $config
     */
    public function connect() {
        if($this->connection == null && $this->channel == null) {
            $this->connection = new AMQPStreamConnection(
                $this->config['host'],
                $this->config['port'],
                $this->config['username'],
                $this->config['password']
            );

            $this->channel = $this->connection->channel();
        }

        if($this->channel == null && $this->connection instanceof AMQPStreamConnection) {
            $this->channel = $this->connection->channel();
        }
    }

    /**
     * Disconnect to queue
     */
    public function disconnect() {
        $this->channel->close();
        $this->connection->close();

        $this->channel    = null;
        $this->connection = null;

        return true;
    }
}

<?php

namespace Ecw\Grunt\Connectors;

use Pheanstalk\Pheanstalk;


class BeanstalkdConnector implements ConnectorInterface
{
    /**
     * Connection to queue
     * @var mixed
     */
    protected $beanstalkd;

    /**
     * Beanstalkd configuration
     * @var  mixed
     */
    protected $config;


    /**
     * Get name of queue service
     */
    public function getConnectorName() {
        return 'Beanstalkd';
    }

    /**
     * Get cofnig of queue service
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Set config of queue service
     */
    public function setConfig(array $config) {
        $this->config = $config;
    }

    /**
     * Get connection to queue service (instantiated object of library)
     */
    public function getConnection() {
        return $this->beanstalkd;
    }

    public function setConnection(Pheanstalk $beanstalkd) {
        $this->beanstalkd = $beanstalkd;
    }

    /**
     * Establishes queue connection
     * @param  array  $config
     */
    public function connect() {
        if($this->beanstalkd == null) {
            $this->beanstalkd = new Pheanstalk(
                $this->config['host'],
                $this->config['port'],
                $this->config['connection_timeout'],
                $this->config['is_persistent']
            );
        }
    }

    /**
     * Disconnect to queue
     */
    public function disconnect() {
        $this->beanstalkd = null;

        return true;
    }
}

<?php

namespace Ecw\Grunt\Connectors;


interface ConnectorInterface
{
    /**
     * Returns naem of queue service
     */
    public function getConnectorName();

    /**
     * Returns queue service config
     */
    public function getConfig();

    /**
     * Set queue service config (host, port, etc)
     * @param  array  $config  [description]
     */
    public function setConfig(array $config);

    /**
     * Returns queue service connection object
     */
    public function getConnection();

    /**
     * Estabishes a connection to queue service
     * @param  array  $confg
     */
    public function connect();

    /**
     * Closes connection to queue service
     */
    public function disconnect();
}

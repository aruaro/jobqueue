<?php

namespace Ecw\Grunt\Publishers;

use Ecw\Grunt\Connectors\ConnectorInterface;
use Ecw\Grunt\Jobs\JobInterface;


interface PublisherInterface
{
    /**
     * Create job from task and task data
     * @param   mixed   $task       object, string, closure, whatever
     * @param   array   $params     data to be passed to the task
     */
    public function createJob($task, array $params);

    /**
     * Send job to queue
     * @param   mixed   $task       object, string, closure, whatever
     * @param   array   $params     data to be passed to the task
     */
    public function send($task, array $params);

    /**
     * Push a job to the queue
     * @param   JobInterface  $job
     */
    public function push(JobInterface $job);
}

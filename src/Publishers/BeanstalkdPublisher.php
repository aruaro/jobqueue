<?php

namespace Ecw\Grunt\Publishers;

use Ecw\Grunt\Jobs\JobInterface;
use Ecw\Grunt\Jobs\BeanstalkdJob;


class BeanstalkdPublisher extends Publisher
{
    /**
     * Create job from task and task data
     * @param   mixed   $task       object, string, closure, whatever
     * @param   array   $params     data to be passed to the task
     */
    public function createJob($task, array $params = []) {
        $job = new BeanstalkdJob();

        $job->setTaskName($task);
        $job->setTaskData($params);
        $job->setLogger($this->logger);

        return $job;
    }

    /**
     * Push task into queue
     */
    public function push(JobInterface $job) {
        try {
            $beanstalkd = $this->getConnector()->getConnection();

            $this->runBeforePush($job);

            $beanstalkd->useTube($this->getQueue());

            $jobId = $beanstalkd->put(
                $job->getJobData(),
                $job->getPriority(),
                $job->getDelay(),
                $job->getTtr()
            );

            $job->setJobId($jobId);

            $this->runAfterPush($job);

            $this->getConnector()->disconnect();

            return $jobId;

        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }
    }
}

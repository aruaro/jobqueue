<?php

namespace Ecw\Grunt\Publishers;

use Ecw\Grunt\Jobs\JobInterface;
use Ecw\Grunt\Jobs\RabbitMQJob;


class RabbitMQPublisher extends Publisher
{

    /**
     * Create job from task and task data
     * @param   mixed   $task       object, string, closure, whatever
     * @param   array   $params     data to be passed to the task
     */
    public function createJob($task, array $params = []) {
        $job = new RabbitMQJob();

        $job->setTaskName($task);
        $job->setTaskData($params);
        $job->setLogger($this->logger);

        return $job;
    }

    /**
     * Push task into queue
     * @param  JobInterface $job
     */
    public function push(JobInterface $job) {
        try {
            $rabbitmq = $this->getConnector()->getConnection();

            $this->runBeforePush($job);

            // $rabbitmq->exchange_declare($this->getQueue(), 'fanout', false, true, false);

            $rabbitmq->basic_publish(
                $job->getRabbitmqJob(),            // message
                '',                            // exchange
                $this->getQueue()              // routing key
            );

            $this->runAfterPush($job);

            return true;
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }
    }

    /**
     * Run things after pushing the job to queue
     * @param   JobInterface  $job
     */
    public function runAfterPush(JobInterface $job) {
        $this->getConnector()->disconnect();
    }
}

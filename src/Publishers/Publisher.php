<?php

namespace Ecw\Grunt\Publishers;

use Ecw\Grunt\Events\EventListener;
use Ecw\Grunt\Connectors\ConnectorInterface;
use Ecw\Grunt\Jobs\JobInterface;


class Publisher implements PublisherInterface
{
    /**
     * Logger
     * @var  EventListener
     */
    protected $logger;

    /**
     * Connector instance
     * @var  ConnectorInterface
     */
    protected $connector;

    /**
     * Queue name
     * @var string
     */
    protected $queue;


    public function __construct(ConnectorInterface $connector, $queue) {
        $this->connector = $connector;
        $this->queue     = $queue;
    }

    /**
     * Gets queue connection
     * @return  ConnectorInterface
     */
    public function getConnector() {
        return $this->connector;
    }

    /**
     * Sets queue connection
     * @param  ConnectorInterface  $connector
     */
    public function setConnector(ConnectorInterface $connector) {
        $this->connector = $connector;
    }

    /**
     * Returns queue name
     * @return  string
     */
    public function getQueue() {
        return $this->queue;
    }

    /**
     * Set name of queue
     * @param  string  $queue
     */
    public function setQueue($queue) {
        $this->queue = $queue;
    }

    /**
     * Sets the event logger and listener
     */
    public function setLogger(EventListener $logger) {
        $this->logger = $logger;
    }

    /**
     * Converts task to job readable by the queue
     * @param   string  $task    namespace of task name
     * @param   array   $params  params to be used by task
     */
    public function send($task, array $params = []) {
        $job = $this->createJob($task, $params);

        if($job->validateJob() !== true) {
            // fire event
            return false;
        }

        if($job->resolveTask() === true) {
            if($this->push($job)) {
                $this->logger->fire(new \Ecw\Grunt\Events\JobPublishedEvent($job));

                return true;
            }
        } else {
            $this->logger->fire(new \Ecw\Grunt\Events\JobNotPublishedEvent($job));

            return false;
        }
    }

    /**
     * Create job from task and task data
     * @param   mixed   $task       object, string, closure, whatever
     * @param   array   $params     data to be passed to the task
     */
    public function createJob($task, array $params = []) {
        return JobInterface;
    }

    /**
     * Push task into queue
     * @param  JobInterface $job
     */
    public function push(JobInterface $job) {
        return true;
    }

    /**
     * Run some things before really pushing the job to queue
     * @param  JobInterface $job
     */
    protected function runBeforePush(JobInterface $job) {
        return true;
    }


    /**
     * Run things job is pushed into queue
     * @param  JobInterface $job
     */
    protected function runAfterPush(JobInterface $job) {
        return true;
    }
}

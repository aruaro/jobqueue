<?php

namespace Ecw\Grunt\Publishers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


class PublisherServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers the publisher service, instantiates all required objects, etc.
     * It's done this way:
     *     1. So that connections are reused and memory is conserved
     *     2. So that queueing services can be easily changed / configured
     *
     * @param   Container  $container  Pimple container
     */
    public function register(Container $container) {
        $config = $container['config.queue'];

        switch($config['type']) {
            case 'beanstalkd':
                // connector
                $container['queue.connector'] = function($c) use($config) {
                    $connector = new \Ecw\Grunt\Connectors\BeanstalkdConnector();

                    $connector->setConfig($config);
                    $connector->connect();

                    return $connector;
                };

                // publisher
                $container['queue.publisher'] = function($c) use($config) {
                    $publisher = new \Ecw\Grunt\Publishers\BeanstalkdPublisher(
                                    $c['queue.connector'],
                                    $config['name']
                                );

                    $publisher->setLogger($c['logger']);

                    return $publisher;
                };
            break;

            case 'rabbitmq':
                // connector
                $container['queue.connector'] = function($c) use($config) {
                    $connector = new \Ecw\Grunt\Connectors\RabbitMQConnector();

                    $connector->setConfig($config);
                    $connector->connect();

                    return $connector;
                };

                // publisher
                $container['queue.publisher'] = function($c) use($config) {
                    $publisher = new \Ecw\Grunt\Publishers\RabbitMQPublisher(
                                    $c['queue.connector'],
                                    $config['name']
                                );

                    $publisher->setLogger($c['logger']);

                    return $publisher;
                };
            break;
        }
    }
}

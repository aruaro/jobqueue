<?php

namespace Ecw\Grunt\Jobs;


interface JobInterface
{
    /**
     * Run checks to make sure job is valid
     */
    public function validateJob();

    /**
     * Execute job
     */
    public function execute();

    /**
     * Terminate job, forcefully or otherwise
     */
    public function terminate();
}

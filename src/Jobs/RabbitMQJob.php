<?php

namespace Ecw\Grunt\Jobs;

use PhpAmqpLib\Message\AMQPMessage;


class RabbitMQJob extends Job
{
    /**
     * RabbitMQ message object
     * @var  AMQPMessage
     */
    protected $rabbitmqJob;

    /**
     * Get RabbitMQ message object
     */
    public function getRabbitmqJob() {
        if(($this->rabbitmqJob instanceof AMQPMessage) === false) {
            $this->rabbitmqJob = new AMQPMessage(
                json_encode([
                    'taskClass' => $this->getTaskName(),
                    'taskData'  => $this->getTaskData(),
                ]),
                ['delivery_mode' => 2]
            );
        }

        return $this->rabbitmqJob;
    }

    /**
     * Set RabbitMQ message object
     * @param  AMQPMessage  $rabbitmqJob
     */
    public function setRabbitmqJob(AMQPMessage $rabbitmqJob) {
        $data = json_decode($rabbitmqJob->body, true);

        $this->setTaskName($data['taskClass']);
        $this->setTaskData($data['taskData']);

        // just to fill in job data
        $this->setJobData($data);

        $this->rabbitmqJob = $rabbitmqJob;
    }

    /**
     * Get job data formatted for RabbitMQ
     */
    public function getJobData() {
        if(is_array($this->jobData)) {
            return json_encode($this->jobData);
        } else {
            return $this->jobData;
        }
    }

    /**
     * Run checks to make sure job is valid
     * @param  JobAbstract  $job
     */
    public function validateJob() {
        return true;
    }

    public function execute() {
        if($this->getTaskData() == null) {
            throw new \InvalidArgumentException('Task data is null.');

            return false;
        }

        return $this->taskCallable->execute($this->getTaskData());
    }
}
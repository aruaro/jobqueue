<?php

namespace Ecw\Grunt\Jobs;

use Pheanstalk\PheanstalkInterface;
use Pheanstalk\Job as PheanstalkJob;


class BeanstalkdJob extends Job
{
    protected $pheanstalkJob;

    /**
     * Job priority - lower means higher priority
     * @var integer
     */
    protected $priority = 1;

    /**
     * Time until job starts running?
     * @var DateTime
     */
    protected $delay;

    /**
     * Job expiration date
     * @var  DateTime
     */
    protected $ttr;

    /**
     * Tells if job is delayed
     * @var boolean
     */
    protected $isDelayed = false;


    public function getPheanstalkJob() {
        return $this->pheanstalkJob;
    }

    public function setPheanstalkJob(PheanstalkJob $pheanstalkJob) {
        $data = json_decode($pheanstalkJob->getData(), true);

        $this->setJobId($pheanstalkJob->getId());
        $this->setTaskName($data['taskClass']);
        $this->setTaskData($data['taskData']);

        // just to fill in job data
        $this->setJobData($data);

        $this->pheanstalkJob = $pheanstalkJob;
    }

    public function getJobId() {
        return $this->jobId;
    }

    public function setJobId($jobId) {
        $this->jobId = $jobId;
    }

    public function getJobData() {
        if(is_array($this->jobData)) {
            return json_encode($this->jobData);
        } else {
            return $this->jobData;
        }
    }

    public function getPriority() {
        return $this->priority;
    }

    public function setPriority($priority) {
        $this->priority = $priority;
    }

    public function getDelay() {
        return $this->delay;
    }

    public function setDelay($delay) {
        $this->delay = $delay;
    }

    public function getTtr() {
        return $this->ttr;
    }

    public function setTtr($ttr) {
        $this->ttr = $ttr;
    }

    /**
     * Run checks to make sure job is valid
     * @param  JobAbstract  $job
     */
    public function validateJob() {
        return true;
    }

    /**
     * Run the job
     */
    public function execute() {
        if($this->getTaskData() == null) {
            throw new \InvalidArgumentException('Task data is null.');

            return false;
        }

        return $this->taskCallable->execute($this->getTaskData());
    }
}

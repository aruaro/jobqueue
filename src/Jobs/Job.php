<?php

namespace Ecw\Grunt\Jobs;

use Ecw\Grunt\Events\EventListener;


class Job implements JobInterface
{
    /**
     * Event logger
     * @var  EventListener
     */
    protected $logger;

    /**
     * Task name
     * @var string
     */
    protected $taskName;

    /**
     * Callable for the task
     * @var  mixed
     */
    protected $taskCallable;

    /**
     * Data passed to task
     * @var mixed
     */
    protected $taskData;

    /**
     * Data passed to queue/messaging service (readable to them)
     * @var  mixed
     */
    protected $jobData;

    /**
     * Number of tries the job has been run
     * @var integer
     */
    protected $tries = 0;

    /**
     * Number of retries before a failed job gets kicked out of the publisher
     * @var integer
     */
    protected $maxRetries = 1;

    /**
     * Time when job is first pushed into publisher
     * @var DateTime
     */
    protected $startTime = 0;

    /**
     * Sets the event logger and listener
     */
    public function setLogger(EventListener $logger) {
        $this->logger = $logger;
    }


    public function getTaskName() {
        return $this->taskName;
    }

    public function setTaskName($taskName) {
        $this->taskName = $taskName;
    }

    public function getTaskData() {
        return $this->taskData;
    }

    public function setTaskData($taskData) {
        $this->taskData = $taskData;
    }

    public function getTaskCallable() {
        return $this->taskCallable;
    }

    public function setTaskCallable($taskCallable) {
        $this->taskCallable = $taskCallable;
    }

    public function getJobData() {
        return $this->jobData;
    }

    public function setJobData($jobData) {
        $this->jobData = $jobData;
    }

    public function getTries() {
        return $this->tries;
    }

    public function getRunTime() {
        return microtime(true) - $this->startTime;
    }

    /**
     * Tries to determine task type (object, string, whatever)
     * If object, instantiate. If other, print the damn thing?
     */
    public function resolveTask() {
        $taskData = $this->getTaskData();
        $taskName = $this->getTaskName();

        if(class_exists($taskName)) {
            if(!method_exists($taskName, 'execute')) {
                // fire event
            } else {
                $this->taskCallable = new $taskName();

                $this->taskCallable->setLogger($this->logger);

                return true;
            }
        }

         // fire event

        return false;
    }

    /**
     * Run checks to make sure job is valid
     * @param  JobAbstract  $job
     */
    public function validateJob() {
        return true;
    }

    /**
     * Run the job
     */
    public function execute() {
        return true;
    }

    /**
     * Send a signal or whatever to terminate the job but not remove it from queue
     */
    public function terminate() {
        return true;
    }
}

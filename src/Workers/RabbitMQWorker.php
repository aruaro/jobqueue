<?php

namespace Ecw\Grunt\Workers;

use Ecw\Grunt\Jobs\JobInterface;
use Ecw\Grunt\Jobs\RabbitMQJob;

use PhpAmqpLib\Message\AMQPMessage;


class RabbitMQWorker extends Worker
{
    /**
     * Listen for incoming jobs
     */
    public function listen() {
        try {
            list($rabbitmq, $rabbitmqQueue) = $this->initRabbitmq();

            $rabbitmq->basic_consume(
                $this->getQueue(),          // rabbitmq generated queue name
                '',                         // consumer tag?
                false,                      // no local
                false,                      // no ack (we should ack)
                false,                      // exclusive
                false,                      // no wait
                [$this, 'rabbitmqCallback'] // callback
            );

            $this->logger->fire(new \Ecw\Grunt\Events\WorkerConnectedEvent($this));

            while(count($rabbitmq->callbacks)) {
                $rabbitmq->wait();

                usleep($this->getListenDelay());
            }
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }
    }

    /**
     * RabbitMQ callback method
     * @param   AMQPMessage  $msg
     */
    public function rabbitmqCallback(AMQPMessage $msg) {
        $job = new RabbitMQJob();

        $job->setLogger($this->logger);
        $job->setRabbitmqJob($msg);

        $this->logger->fire(new \Ecw\Grunt\Events\JobFetchedEvent($job));

        if($job->validateJob() === false) {
            $this->logger->fire(new \Ecw\Grunt\Events\JobTerminatedEvent($job));

            continue;
        }

        $this->pop($job);
    }

    /**
     * Decide what to do with jobs queued
     * @param   JobInterface  $job
     */
    public function pop(JobInterface $job) {
        $rabbitmq = $job->getRabbitMqJob()->delivery_info['channel'];
        $status   = false;

        $this->runBeforePop($job);

        try {
            $this->logger->fire(new \Ecw\Grunt\Events\JobExecutingEvent($job));

            $jobResolvedTask = $job->resolveTask();
            $jobExecuted     = $job->execute();

            if($jobResolvedTask === true && $jobExecuted === true) {
                $this->logger->fire(new \Ecw\Grunt\Events\JobFinishedEvent($job));

                $rabbitmq->basic_ack(
                    $job->getRabbitMqJob()->delivery_info['delivery_tag']
                );

                $status = true;
            } else {
                // TODO: check for TTL
                $rabbitmq->basic_cancel(
                    $job->getRabbitMqJob()->delivery_info['consumer_tag']
                );

                $this->logger->fire(new \Ecw\Grunt\Events\JobTerminatedEvent($job));

                $status = false;
            }
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }

        $this->runAfterPop($job);

        return $status;
    }

    /**
     * Run things after job is released from queue
     * @param   JobInterface  $job
     */
    public function runAfterPop(JobInterface $job) {

    }

    /**
     * Flush all queued jobs
     */
    public function flushAll() {
        try {
            list($rabbitmq, $rabbitmqQueue) = $this->initRabbitmq();

            $logger = $this->logger;

            $callback = function($msg) use($logger) {
                $msg->delivery_info['channel']->basic_ack(
                    $msg->delivery_info['consumer_tag']
                );

                $logger->fire(
                    new \Ecw\Grunt\Events\JobDeletedEvent($msg)
                );
            };

            $rabbitmq->basic_consume(
                $rabbitmqQueue,         // rabbitmq generated queue name
                '',                     // consumer tag?
                false,                  // no local
                false,                  // no ack (we should ack)
                false,                  // exclusive
                false,                  // no wait
                $callback
            );

            while(count($rabbitmq->callbacks)) {
                $this->logger->fire(new \Ecw\Grunt\Events\WorkerConnectedEvent($this));

                $rabbitmq->wait();

                usleep($this->getListenDelay());
            }

            $this->getConnector()->disconnect();

        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }
    }

    /**
     * Initialize and configure RabbitMQ exchange and queue
     */
    private function initRabbitmq() {
        $rabbitmq = $this->getConnector()->getConnection();

        // $rabbitmq->exchange_declare(
        //     $this->getQueue(),      // queue name
        //     'fanout',               // exchange type
        //     false,                  // passive
        //     true,                   // durable
        //     false                   // auto delete
        // );

        list($rabbitmqQueue, ,) = $rabbitmq->queue_declare(
            $this->getQueue(),      // queue name
            false,                  // passive
            true,                   // durable
            false                   // exclusive
        );

        // $rabbitmq->queue_bind($rabbitmqQueue, $this->getQueue());

        $rabbitmq->basic_qos(null, 1, null);

        return [$rabbitmq, $rabbitmqQueue];
    }
}

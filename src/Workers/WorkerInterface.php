<?php

namespace Ecw\Grunt\Workers;

use Ecw\Grunt\Connectors\ConnectorInterface;
use Ecw\Grunt\Publishers\PublisherAbstract;
use Ecw\Grunt\Jobs\JobInterface;


interface WorkerInterface
{
    /**
     * Listen for incoming jobs and process them
     */
    public function listen();

    /**
     * Remove a job from the queue
     * @param   JobInterface  $job
     */
    public function pop(JobInterface $job);
}

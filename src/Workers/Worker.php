<?php

namespace Ecw\Grunt\Workers;

use Ecw\Grunt\Events\EventListener;
use Ecw\Grunt\Connectors\ConnectorInterface;
use Ecw\Grunt\Jobs\JobInterface;


class Worker implements WorkerInterface
{
    /**
     * Logger
     * @var  EventListener
     */
    protected $logger;

    /**
     * Connector instance
     * @var  ConnectorInterface
     */
    protected $connector;

    /**
     * Queue name
     * @var string
     */
    protected $queue;

    /**
     * Sleep time in between job execution
     * @var  int
     */
    protected $listenDelay;


    public function __construct(ConnectorInterface $connector, $queue) {
        $this->connector = $connector;
        $this->queue     = $queue;

        $this->listenDelay = 0;
    }

    /**
     * Gets queue connection
     * @return  ConnectorInterface
     */
    public function getConnector() {
        return $this->connector;
    }

    /**
     * Sets queue connection
     * @param  ConnectorInterface  $connector
     */
    public function setConnector(ConnectorInterface $connector) {
        $this->connector = $connector;
    }

    /**
     * Returns queue name
     * @return  string
     */
    public function getQueue() {
        return $this->queue;
    }

    /**
     * Set name of queue
     * @param  string  $queue
     */
    public function setQueue($queue) {
        $this->queue = $queue;
    }

    /**
     * Sets the event logger and listener
     */
    public function setLogger(EventListener $logger) {
        $this->logger = $logger;
    }

    /**
     * Get sleep time in between job executions
     */
    public function getListenDelay() {
        return $this->listenDelay;
    }

    /**
     * Set sleep time in between job executions
     * @param  int  $listenDelay
     */
    public function setListenDelay($listenDelay) {
        $this->listenDelay = $listenDelay;
    }

    /**
     * Listen for incoming jobs from queue
     */
    public function listen() {
        return true;
    }

    /**
     * Remove task from queue
     * @param  JobInterface $job
     */
    public function pop(JobInterface $job) {
        return true;
    }

    /**
     * Run some things before removing the job to queue
     * @param  JobInterface $job
     */
    protected function runBeforePop(JobInterface $job) {
        return true;
    }

    /**
     * Run things job is removed from queue
     * @param  JobInterface $job
     */
    protected function runAfterPop(JobInterface $job) {
        return true;
    }
}

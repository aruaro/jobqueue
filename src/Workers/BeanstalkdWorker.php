<?php

namespace Ecw\Grunt\Workers;

use Ecw\Grunt\Jobs\JobInterface;
use Ecw\Grunt\Jobs\BeanstalkdJob;


class BeanstalkdWorker extends Worker
{
    /**
     * Listen for incoming jobs
     */
    public function listen() {
        try {
            $beanstalkd = $this->getConnector()->getConnection();

            $beanstalkd->watchOnly($this->getQueue());

            $this->logger->fire(new \Ecw\Grunt\Events\WorkerConnectedEvent($this));
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }

        try {
            while(true) {
                $job           = new BeanstalkdJob();
                $pheanstalkJob = $beanstalkd->reserve();

                if(!($pheanstalkJob instanceof \Pheanstalk\Job)) {
                    usleep($this->getListenDelay());

                    continue;
                }

                $job->setLogger($this->logger);
                $job->setPheanstalkJob($pheanstalkJob);

                $this->logger->fire(new \Ecw\Grunt\Events\JobFetchedEvent($job));

                if($job->validateJob() === false) {
                    $this->logger->fire(new \Ecw\Grunt\Events\JobTerminatedEvent($job));

                    continue;
                }

                $this->pop($job);

                usleep($this->getListenDelay());
            }
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }
    }

    /**
     * Decide what to do with jobs queued
     * @param   JobInterface  $job
     */
    public function pop(JobInterface $job) {
        $status = false;

        $this->runBeforePop($job);

        try {
            $this->logger->fire(new \Ecw\Grunt\Events\JobExecutingEvent($job));

            if($job->resolveTask() === true && $job->execute() === true) {
                $this->getConnector()->getConnection()->delete($job->getPheanstalkJob());

                $this->logger->fire(new \Ecw\Grunt\Events\JobFinishedEvent($job));

                $status = true;
            } else {
                // TODO: check for TTL
                $this->getConnector()->getConnection()->release($job->getPheanstalkJob());

                $status = false;
            }
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }


        $this->runAfterPop($job);

        return $status;
    }

    /**
     * Flush all queued jobs
     */
    public function flushAll() {
        try {
            $beanstalkd = $this->getConnector()->getConnection();

            $beanstalkd->watchOnly($this->getQueue());

            while($this->getQueuedJobs() > 0) {
                $pheanstalkJob = $beanstalkd->reserve();

                $beanstalkd->delete($pheanstalkJob);

                $this->logger->fire(new \Ecw\Grunt\Events\JobDeletedEvent($pheanstalkJob));

                usleep($this->getListenDelay());
            }
        } catch(\Exception $e) {
            $this->logger->handleException($e);
        }
    }

    /**
     * Get number of queued jobs
     */
    private function getQueuedJobs() {
        return intval(
            $this->getConnector()
                ->getConnection()
                ->statsTube($this->getQueue())['current-jobs-ready']
            );

    }
}

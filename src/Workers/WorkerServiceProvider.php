<?php

namespace Ecw\Grunt\Workers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


class WorkerServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers the worker service, instantiates all required objects, etc.
     * It's done this way:
     *     1. So that connections are reused and memory is conserved
     *     2. So that queueing services can be easily changed / configured
     *
     * @param   Container  $container  Pimple container
     */
    public function register(Container $container) {
        $config = $container['config.queue'];

        switch($config['type']) {
            case 'beanstalkd':
                $container['queue.connector'] = function($c) use($config) {
                    $connector = new \Ecw\Grunt\Connectors\BeanstalkdConnector($config);

                    $connector->setConfig($config);
                    $connector->connect();

                    return $connector;
                };

                $container['queue.worker'] = function($c) use($config) {
                    $worker = new \Ecw\Grunt\Workers\BeanstalkdWorker($c['queue.connector'], $config['name']);

                    $worker->setLogger($c['logger']);
                    $worker->setListenDelay($c['config.queue']['listen_delay']);

                    return $worker;
                };
            break;

            case 'rabbitmq':
                $container['queue.connector'] = function($c) use($config) {
                    $connector = new \Ecw\Grunt\Connectors\RabbitMQConnector($config);

                    $connector->setConfig($config);
                    $connector->connect();

                    return $connector;
                };

                $container['queue.worker'] = function($c) use($config) {
                    $worker = new \Ecw\Grunt\Workers\RabbitMQWorker($c['queue.connector'], $config['name']);

                    $worker->setLogger($c['logger']);
                    $worker->setListenDelay($c['config.queue']['listen_delay']);

                    return $worker;
                };
            break;
        }
    }
}

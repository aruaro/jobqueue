<?php

namespace Ecw\Grunt\Events;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


class EventServiceProvider implements ServiceProviderInterface
{
    public function register(Container $container) {
        $container['logger'] = function($c) {
            $listener = new EventListener($c['logger.channel']);
            $handlers = [];

            $handlers[] = [
                'handler'    => new \Monolog\Handler\StreamHandler($c['logger.path']),
                'formatters' => [
                    new EventLineFormatter(
                        "[%datetime%][%channel%][%level_name%] %message%",
                        null,
                        true,
                        true
                    ),
                ]
            ];

            $processors = [
                new \Monolog\Processor\MemoryUsageProcessor(),
                new \Monolog\Processor\PsrLogMessageProcessor(),
                new \Monolog\Processor\IntrospectionProcessor(),
                new \Monolog\Processor\TagProcessor()
            ];

            $handlers = array_merge($handlers, $c['logger.handlers']);

            foreach($processors as $processor) {
                $listener->registerProcessor($processor);
            }

            foreach($handlers as $handler) {
                $listener->registerHandler($handler);
            }

            return $listener;
        };
    }
}

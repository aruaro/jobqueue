<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Ecw\Grunt\Publishers\PublisherInterface;
use Ecw\Grunt\Events\EventInterface;


class PublisherEvent implements EventInterface
{
    public $connection;
    public $queue;


    public function __construct(PublisherInterface $publisher) {
        $this->connection = $publisher->getConnector()->getConnectorName();
        $this->queue      = $publisher->getQueue();
    }

    public function getSeverity() {
        return Logger::INFO;
    }

    public function getMessage() {
        return '';
    }

    public function getContext() {
        return [
            'connection' => $this->connection,
            'queue'      => $this->queue
        ];
    }
}

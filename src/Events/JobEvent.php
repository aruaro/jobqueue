<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Ecw\Grunt\Jobs\JobInterface;


class JobEvent implements EventInterface
{
    public $taskName;
    public $taskData;
    public $extra;


    public function __construct(JobInterface $job, $extra = null) {
        $this->taskName = trim(str_replace('Ecw\\Grunt\\', '', $job->getTaskName()), '\\');
        $this->taskData = $job->getTaskData();
        $this->extra    = $extra;
    }

    public function getSeverity() {
        return Logger::INFO;
    }

    public function getMessage() {
        return $this->taskName;
    }

    public function getContext() {
        return [
            'task_name' => $this->taskName,
            'task_data' => $this->taskData,
            'extra'     => $this->extra
        ];
    }

    public function setExtra($extra) {
        $this->extra = $extra;
    }
}

<?php

namespace Ecw\Grunt\Events;


class JobTerminatedEvent extends JobEvent
{
    public function getSeverity() {
        return Logger::WARNING;
    }
}

<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Ecw\Grunt\Tasks\TaskInterface;


class TaskEvent implements EventInterface
{
    public $taskName;
    public $extra;


    public function __construct(TaskInterface $task, $extra = null) {
        $this->taskName = str_replace('Ecw\\Grunt\\', '', get_class($task));
        $this->extra    = $extra;
    }

    public function getSeverity() {
        return Logger::INFO;
    }

    public function getMessage() {
        return '';
    }

    public function getContext() {
        return [
            'task_name'  => $this->taskName,
            'extra'      => $this->extra
        ];
    }

    public function setExtra($extra) {
        $this->extra = $extra;
    }
}

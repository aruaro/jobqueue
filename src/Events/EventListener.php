<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Monolog\ErrorHandler;


class EventListener
{
    protected $handlers = [];

    protected $processors = [];

    private $logger;

    private $errorHandler;


    public function __construct($channel = null) {
        $this->logger = new Logger($channel);

        $this->errorHandler = new ErrorHandler($this->logger);

        $this->errorHandler->registerErrorHandler([], false);
        $this->errorHandler->registerExceptionHandler();
        $this->errorHandler->registerFatalHandler();

        $whoops = new \Whoops\Run();
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

        if (isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] == 'application/json') {
            $jsonHandler = new \Whoops\Handler\JsonResponseHandler();
            $jsonHandler->setJsonApi(true);
            $whoops->pushHandler($jsonHandler);
        }

        $whoops->register();
    }

    /**
     * Adds a log handdler to Monolog, along with its formatters
     * @param   mixed  $handler  handler + formatters
     */
    public function registerHandler($handler) {
        foreach($handler['formatters'] as $formatter) {
            $handler['handler']->setFormatter($formatter);
        }

        $this->logger->pushHandler($handler['handler']);

        $this->handlers[] = $handler;
    }

    /**
     * Adds a processor to Monolog. Here we'll add CPU usage, GIT commit, etc.
     * @param   mixed  $processor  processor
     */
    public function registerProcessor($processor) {
        $this->logger->pushProcessor($processor);
    }

    /**
     * Fires an event, sends it to Monolog, sends it to all handlers
     * @param   EventInterface  $event  Event object
     */
    public function fire(EventInterface $event) {
        $this->logger->log(
            $event->getSeverity(),
            sprintf('%s: %s', str_replace('Ecw\\Grunt\\Events\\', '', get_class($event)), $event->getMessage()),
            ['event' => $event->getContext()]
        );
    }

    /**
     * Custom exception handler
     * @param   \Exception  $e
     */
    public function handleException(\Exception $e) {
        $this->logger->log(
            \Psr\Log\LogLevel::ERROR,
            sprintf('Exception %s: "%s" at %s line %s', get_class($e), $e->getMessage(), $e->getFile(), $e->getLine()),
            array('exception' => $e)
        );
    }
}

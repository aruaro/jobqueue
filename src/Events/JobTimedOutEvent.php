<?php

namespace Ecw\Grunt\Events;


class JobTimedOutEvent extends JobEvent
{
    public function getSeverity() {
        return Logger::WARNING;
    }
}

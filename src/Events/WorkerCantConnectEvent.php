<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Ecw\Grunt\Connector\ConnectorInterface;
use Ecw\Grunt\Workers\WorkerInterface;


class WorkerCantConnectEvent extends WorkerEvent
{
    public $exception;


    public function __construct(ConnectorInterface $connection, WorkerInterface $worker, Exception $e) {
        parent::__construct($connection, $worker);

        $this->exception = $e;
    }

    public function getSeverity() {
        Logger::ERROR;
    }
}

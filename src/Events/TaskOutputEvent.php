<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;


class TaskOutputEvent extends TaskEvent
{
    public function getMessage() {
        if(isset($this->extra['task_output'])) {
            return sprintf('%s: %s', $this->taskName, $this->extra['task_output']);
        } else {
            return '';
        }
    }
}

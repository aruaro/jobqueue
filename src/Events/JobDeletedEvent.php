<?php

namespace Ecw\Grunt\Events;


class JobDeletedEvent extends JobEvent
{
    public $job;
    public $extra;


    public function __construct(\Pheanstalk\Job $job, $extra = null) {
        $this->job   = $job;
        $this->extra = $extra;
    }

    public function getSeverity() {
        return \Monolog\Logger::INFO;
    }

    public function getContext() {
        return [
            'job'   => $this->job,
            'extra' => $this->extra
        ];
    }

    public function setExtra($extra) {
        $this->extra = $extra;
    }
}

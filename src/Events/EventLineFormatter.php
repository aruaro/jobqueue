<?php

namespace Ecw\Grunt\Events;

use Monolog\Formatter\LineFormatter;


class EventLineFormatter extends LineFormatter
{
    public function format(array $record)
    {
        // TODO: parse EventInterface instances to human readable format

        return parent::format($record);
    }
}

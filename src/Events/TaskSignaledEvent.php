<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;


class TaskTerminatedEvent extends TaskEvent
{
    public function getSeverity() {
        return Logger::WARNING;
    }
}

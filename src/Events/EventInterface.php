<?php

namespace Ecw\Grunt\Events;


interface EventInterface
{
    public function getSeverity();

    public function getMessage();

    public function getContext();
}

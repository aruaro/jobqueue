<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Ecw\Grunt\Connector\ConnectorInterface;
use Ecw\Grunt\Publishers\PublisherInterface;


class PublisherCantConnectEvent extends PublisherEvent
{
    public $exception;


    public function __construct(ConnectorInterface $connection, PublisherInterface $publisher, Exception $e) {
        parent::__construct($connection, $publisher);

        $this->exception = $e;
    }

    public function getSeverity() {
        return Logger::ERROR;
    }
}

<?php

namespace Ecw\Grunt\Events;

use Monolog\Logger;
use Ecw\Grunt\Workers\WorkerInterface;
use Ecw\Grunt\Events\EventInterface;


class WorkerEvent implements EventInterface
{
    public $connection;
    public $queue;


    public function __construct(WorkerInterface $worker) {
        $this->connection = $worker->getConnector()->getConnectorName();
        $this->queue      = $worker->getQueue();
    }

    public function getSeverity() {
        return Logger::INFO;
    }

    public function getMessage() {
        return '';
    }

    public function getContext() {
        return [
            'connection' => $this->connection,
            'queue'      => $this->queue
        ];
    }
}
